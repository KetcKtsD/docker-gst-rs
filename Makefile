TAG := ketcktsd/gst-rs:1.82.0-bionic

all: build upload

build:
	docker buildx build -f Dockerfile . --platform linux/arm64 --tag $(TAG)

upload:
	docker push $(TAG)
