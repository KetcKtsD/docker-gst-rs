FROM ubuntu:bionic AS sccache-install

ENV SCCACHE_VERSION='0.8.2'
ENV SCCACHE_URL="https://github.com/mozilla/sccache/releases/download/v${SCCACHE_VERSION}/sccache-v${SCCACHE_VERSION}-x86_64-unknown-linux-musl.tar.gz" \
    SCCACHE_PATH='/usr/local/bin/sccache'

RUN \
    --mount=type=cache,target=/var/cache/apt,sharing=locked \
    apt-get clean && \
    apt-get update && \
    apt-get install -y curl gzip && \
    curl -o /tmp/sccache.tar.gz -L $SCCACHE_URL &&  \
    tar xf /tmp/sccache.tar.gz -C /tmp && \
    ls -la /tmp/sccache-v${SCCACHE_VERSION}-x86_64-unknown-linux-musl && \
    mv /tmp/sccache-v${SCCACHE_VERSION}-x86_64-unknown-linux-musl/sccache /usr/local/bin/ && \
    chmod +x $SCCACHE_PATH

FROM ubuntu:bionic AS rust-install

ENV RS_VERSION='1.82.0'

RUN \
    --mount=type=cache,target=/var/cache/apt,sharing=locked \
    apt-get clean && \
    apt-get update && \
    apt-get install -y curl && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o rustup-install.sh && \
    chmod +x rustup-install.sh && \
    ./rustup-install.sh -y --default-toolchain=${RS_VERSION} && \
    rm rustup-install.sh

ENV PATH=/root/.cargo/bin:$PATH
RUN rustup update

FROM ubuntu:bionic

COPY --from=sccache-install /usr/local/bin/sccache /usr/local/bin/sccache
COPY --from=rust-install /root/.cargo/ /root/.cargo/
COPY --from=rust-install /root/.rustup/ /root/.rustup/

ARG DEBIAN_FRONTEND=noninteractive

RUN \
   --mount=type=cache,target=/var/cache/apt,sharing=locked \
    apt-get clean && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y gcc  \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-libav \
    libgstrtspserver-1.0-dev \
    libges-1.0-dev && \
    rm -rf /var/lib/apt/lists/*

ENV CARGO_HOME=/var/cache/cargo \
    CARGO_NET_GIT_FETCH_WITH_CLI=true
ENV RUSTC_WRAPPER=/usr/local/bin/sccache \
    SCCACHE_DIR=/var/cache/sccache
ENV PATH=/root/.cargo/bin:$PATH
